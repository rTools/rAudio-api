var winston = require("winston");
winston.level = "debug";
var system = require("./libs/system/system.js");

system.init(function(){
	require("./libs/mpd/status-watcher.js").watch();
});
