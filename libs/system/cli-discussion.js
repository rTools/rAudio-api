var scanf = require("scanf");
var chalk = require("chalk");
var log = console.log; //eslint-disable-line
var model = require("./conf-model.js").model;



/**model is a key, a default value, or maybe a array of children   */
function discussConfElem(modelObj){
  // 2 cases :
  // ---- value is a array, that means the cli
  // does NOT ask the value
  // ---- value is not a array, that means modelObj
  // is a parameter and not a category : the cli asks the value

	var confElement = {};
	if (modelObj.value instanceof Array){
		confElement[modelObj.name] = discussConfArray(modelObj);
	}
	else {
		const val = cliAsk(modelObj);
		if(val){
			confElement[modelObj.name] = val ;
		}
	}
	return confElement;
}

function discussConfArray(modelObj){
	var config = {};
	if(modelObj.description){
		for(let i = 0; i < modelObj.description.length; i++){
			log(chalk.green.bold(`${modelObj.description[i]}`));
		}
	}
	for (var i = 0; i < modelObj.value.length; i++){
		if(!cliSkip(config, modelObj.value[i])){
			const confElem = discussConfElem(modelObj.value[i]);
			for (var key in confElem){// didn't find a better way to 'concat' js object
				config[key] = confElem[key];
			}
		}
	}
	return config;
}

function cliSkip(parentConfig, child){
	if (child.skipped){
		for(let i = 0; i < child.skipped.length; i++){
			let key = Object.keys(child.skipped[i])[0];
			let value = child.skipped[i][key];
			if (parentConfig[key] === value) return true;
		}
	}
	return false;
}

function cliAsk(modelObj){
	let value;
	do{
		for(let i = 0; i < modelObj.description.length; i++){
			log(chalk.yellow(`${modelObj.description[i]}${(i === modelObj.description.length - 1) ? printLastLine(modelObj) : ""}`));
		}
		var scan = scanf("%s");
		value = (scan !== "" && scan !== null) ? scan : modelObj.value;
	} while (!validAnswer(modelObj, value));
	return value;
}

function printLastLine(modelObj){
	return `${(modelObj.enum && modelObj.enum.length > 0) ? "[" + modelObj.enum.join("/") + "]" : ""} ${(modelObj.value) ? "(" + modelObj.value + ")" : ""}`;
}

function validAnswer(modelObj, value){
	if (!modelObj.enum || modelObj.enum.indexOf(value) > -1){
		return true;
	} else {
		log( chalk.red.bold("Invalid value !"));
		return false;
	}
}

exports.discussConf = function(){
	return discussConfArray(model);
};
