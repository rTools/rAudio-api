exports.model = {
	value: [
		{
			name : "mpd",
			description: ["--------------MPD settings---------------"],
			value : [
				{
					name: "host",
					description: ["Host of the mpd to connect remotely",
					"works as any host string or IP address"],
					value: "localhost"
				},
				{
					name: "port",
					description: ["port of the mpd to connect remotely"],
					value: "6600"
				},
				{
					name: "password",
					description: ["password of the mpd(no-password)"],
				}
			]
		},
		{
			name : "server",
			description: ["---------NodeJS server  settings---------"],
			value : [
				{
					name: "port",
					description: ["Port the nodeJS server will use.",
					"Please check that the port is allready free"],
					value: "3000"
				}
			]
		},
		{
			name : "auth",
			description: ["---------Authentication settings---------"],
			value : [
				{
					name : "authentication",
					description: ["Do you need a authentication system ?",
						"Possibles cases are : ",
						"none : all access granted to all guest user",
						"admin : must be logged for admin functions only",
						"total : all functions require authentication",
						""
					],
					enum: ["none", "admin", "total"],
					value: "none"
				},
				{
					name: "type",
					description: ["Type of authentication system",
						"Possibles cases are : ",
						"LDAP",
						""
					],
					value: "LDAP",
					enum: ["LDAP"],
					skipped: [{authentication: "none"}]
				},
				{
					name: "host",
					description: ["Host of the authentication server"],
					value: "localhost",
					skipped: [{authentication: "none"}]
				},
				{
					name: "port",
					description: ["Port of the authentication server"],
					value: "0000",
					skipped: [{authentication: "none"}]
				},{
					name: "ldapUserPattern",
					description: [
						"The pattern of ldap user location",
						"Replace the username with #"
					],
					value: "cn=#,ou=users,dc=rBOX,dc=lan",
					skipped: []//no ldap
				},
				{
					name: "member",
					description: ["What group the users must have to access basics functions ? (all users)"],
					skipped: [{authentication: "none"}, {authentication: "admin"}]
				},
				{
					name: "admin",
					description: ["What group the users must have to access admin functions ? (all users)"],
					skipped: [{authentication: "none"}]
				},
			]
		}
	]
};
