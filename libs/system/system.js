var Fiber = require("fibers");
var winston = require("winston");
var client = require("../mpd/client.js");
var config = require("./config.js");
var server = require("./server.js");

function setUpConfig() {
	var fiber = Fiber.current;
	var conf = {};
	setTimeout(function(){
		config.getConfig(function(config){
			conf = config;
			fiber.run();
		});
	});
	Fiber.yield();
	return conf;
}


function setUpMpd(config) {
	var fiber = Fiber.current;
	client.init(config);
	client.on("ready", function() {
		fiber.run();
	});
	Fiber.yield();
}

function setUpServer(config){
	server.init(config);
}


exports.init = function(ready){
	var f = Fiber(function() {
		winston.info("STEP 1/3 : Retrieving the configuration");
		var config = setUpConfig();
		winston.info("STEP 1/3 : Configuration retrieved");
		winston.info("STEP 2/3 : Setting up the MPD interface");
		setUpMpd(config);
		winston.info("STEP 2/3 : MPD connected successfully");
		winston.info("STEP 3/3 : Setting up the express server");
		setUpServer(config);
		winston.info("STEP 3/3 : Express server successfully set up");
		ready();
	});
	f.run();
};
