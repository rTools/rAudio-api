const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const bodyParser = require("body-parser");
const winston = require("winston");
const Fiber = require("fibers");
const cookieSession = require("cookie-session");

const socket = require("../controller/socket.js");


exports.init = function(config){
	var fiber = Fiber.current;
	app.use(bodyParser.json());
	setSession(config);
	http.listen(config.server.port, function () {
		winston.info("Server listening at port %d", config.server.port);
		const rest = require("../controller/rest.js");
		rest(app);
		socket(io);
		fiber.run();
	});
	Fiber.yield();
};

function setSession(config){
	if(config.auth.authentication !== "none"){
		app.use(cookieSession({
			name: "session",
			keys: ["key1", "key2"]
		}));
	}
}
