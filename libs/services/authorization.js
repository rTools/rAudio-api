const config = require("../system/config.js").getConfigInstance();

/**
* determine if the user of the function has the right to execute a endpoint
* @param adminFunction : boolean that means if the function is a admin
*					reserved function. It is used to check the group requirement
* @param req : http request
* @return boolean
*/
exports.isAuthorized = function(adminFunction, req){
	if (config.auth.authentication === "none"){
		return true;
	}
	if (config.auth.authentication === "admin"){
		return (!adminFunction || isAdmin(req));
	}
	// we considering its the last option (total auth)
	return (isAdmin(req) || (!adminFunction && isUser(req)));
};

function isAdmin(req){
	// TODO
	return true;
}

function isUser(req){
	// TODO
	return true;
}
