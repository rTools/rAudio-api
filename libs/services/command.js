var client = require("../mpd/client.js");

exports.go = function (req, res) {
	client.cmd(req.body.cmd, req.body.args, function(err, msg) {
		if (err) res.send({
			msg: "error",
			error: err
		});
		else
		res.send({"msg": msg});
	});
};
