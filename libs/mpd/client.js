var mpd = require("mpd");
var cmd = mpd.cmd;

var instance = null;
exports.init = function (config) {
	if (!instance) {
		instance = require("mpd").connect({
			port: config.mpd.port,
			host: config.mpd.host,
		});
	}
	return instance;
};

exports.on = function (event, callback) {
	instance.on(event, callback);
};

exports.cmd = function (command, args, callback) {
	instance.sendCommand(cmd(command, args), function(err, msg) {
		if (err) throw err;
		callback(err, msg);
	});
};
