const config = require("../system/config.js").getConfigInstance();

var winston = require("winston");


if ( config.auth.type === "LDAP" ){
	const ldap = require("ldapjs");
	var client = ldap.createClient({
		url: `ldap://${config.auth.host}:${config.auth.port}`
	});
}
let tokens = {};

exports.login = (req, res) => {
	if (config.auth.type === "LDAP") {
		client.bind(config.auth.ldapUserPattern.replace("#",req.body.username),
		req.body.password,
		function(err) {
			if(err){
				winston.error(err);
				res.send({
					msg: "error",
					error: err
				});
			} else {
				req.session.token = generateToken();
				res.send({
					msg: "success"
				});
			}
		});
	}
};

exports.logout = (req, res) => {
	delete tokens[req.session.token];
	res.send({
		msg: "success"
	});
};


exports.getUser = (req) => {
	return tokens[req.session.token];
};



function generateToken(username){
	const new_token = Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 45);
	tokens[new_token] = username;
	return new_token;
}
