var command = require("../services/command.js");
var authentication = require("../util/authentication.js");

var config = require("../system/config.js").getConfigInstance();

/**
 * this is the file who groups all the middlewares linked to socket.io event
 *
 * @param : io : the socket.io lib defined in the main file app.js
 *
 * */
exports = module.exports = function(app){
	if (config.auth.authentication !== "none"){
		app.post("/login", authentication.login);
		app.post("/logout", authentication.logout);
	}
	app.post("/cmd",command.go);
};
