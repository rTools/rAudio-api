rAudio - Remote API
===================


Rest API to send commands to a mpd server. Basically you send json and it execute mpd commands. Uses ExpressJs  for server side API,and will most probably use socket.io to notify events.

A front-side client is planned to be implemented, but, because of the single-page-application ecosystem who is just too complicate and will most probably be unstable for the next few years, I'll upload a swagger or else so fell free if you want to implement your own client.